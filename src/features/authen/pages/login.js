import { Button, Card, CardContent, Checkbox, Grid, TextField, Typography } from '@mui/material';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link, useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import { LogoLogin } from '../../../assets/svg/logoSVG';
import 'react-toastify/dist/ReactToastify.css';
function Login() {
    const { register, handleSubmit } = useForm();
    const [getAccount, setGetAccount] = useState();
    const [isCorrect, setIsCorrect] = useState(true);
    const [isUser, setIsUser] = useState(true);
    const isLogin = async () => {
        const getListAccount = await axios({
            method: 'GET',
            url: 'https://63e25f35ad0093bf29ce6905.mockapi.io/users',
        });
        setGetAccount(getListAccount.data);
    };
    useEffect(() => {
        isLogin();
    }, []);
    const navigate = useNavigate();
    const handleLogin = (e) => {
        const checkAccount = getAccount.find((value) => {
            return value.email === e.account && value.password === window.btoa(e.passWord);
        });
        const checkPass = getAccount.find((value) => {
            return value.email === e.account && value.password !== window.btoa(e.passWord);
        });
        const checkUser = getAccount.find((value) => {
            return value.email !== e.account;
        });
        if (checkAccount) {
            localStorage.setItem('GrabAccount', JSON.stringify(checkAccount));
            setIsCorrect(true);
            setIsUser(true);
            toast('success', 'Đăng nhập thành công');

            navigate('/');
        } else if (checkPass) {
            setIsCorrect(false);
        } else if (checkUser) {
            setIsCorrect(false);
            setIsUser(false);
        }
    };
    return (
        <div className="w-full h-[100vh] ">
            <Link to="/" className="border-solid border-[1px] border-b-[#b9b9b951] fixed w-full bg-white">
                <LogoLogin />
            </Link>
            <div className="h-full  bg-[#00b14f]">
                <Grid className="place-items-center grid h-full ">
                    <Card style={{ maxWidth: 450, padding: '20px 5px', margin: '0 auto' }}>
                        <CardContent>
                            <Typography gutterBottom variant="h5">
                                <h5 className="font-semibold text-3xl"> {'Login to Grab'}</h5>
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                                <p className="my-3"> Enter your email number</p>
                            </Typography>
                            <form onSubmit={handleSubmit(handleLogin)}>
                                <Grid container spacing={1}>
                                    <Grid item xs={12}>
                                        {isUser ? (
                                            <TextField
                                                type="email"
                                                placeholder="Enter email"
                                                label="Email"
                                                variant="outlined"
                                                fullWidth
                                                required
                                                color="success"
                                                {...register('account')}
                                            />
                                        ) : (
                                            <TextField
                                                type="email"
                                                placeholder="Enter email"
                                                label="Email"
                                                variant="outlined"
                                                fullWidth
                                                required
                                                color="success"
                                                error
                                                helperText="Email không tồn tại"
                                                {...register('account')}
                                            />
                                        )}
                                    </Grid>
                                    <Grid item xs={12}>
                                        {isCorrect ? (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Password"
                                                type="password"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                {...register('passWord')}
                                            />
                                        ) : (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Password"
                                                type="password"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                {...register('passWord')}
                                                helperText="Password không chính xác"
                                                error
                                            />
                                        )}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Checkbox defaultChecked color="success" />
                                        <span>Remember me on this device for 60 days</span>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            color="success"
                                            fullWidth
                                            className="rounded-3xl h-12"
                                        >
                                            Sign Up
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <span className="cursor-pointer text-[#9b9b9b] hover:underline mr-3">
                                            Fogotten Password?
                                        </span>
                                        <Link
                                            to="/sign-in"
                                            className="cursor-pointer text-[#00b14f] no-underline font-bold hover:underline text-lg"
                                        >
                                            Sign In
                                        </Link>
                                    </Grid>
                                </Grid>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
            </div>
            <ToastContainer />
        </div>
    );
}

export default Login;
