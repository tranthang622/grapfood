import {
    Button,
    Card,
    CardContent,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from '@mui/material';
import { Icon } from '@iconify/react';
import { Link, useNavigate } from 'react-router-dom';
import OTPInput, { ResendOTP } from 'otp-input-react';
import { LogoLogin } from '../../../assets/svg/logoSVG';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useEffect } from 'react';
import { useState } from 'react';
import { firebaseRe } from '../../../features/authen/pages/friebase';
import { RecaptchaVerifier, signInWithPhoneNumber } from 'firebase/auth';
import { Modal } from 'antd';
function SignIn() {
    const [OTP, setOTP] = useState('');
    const [testPhone, setTestPhone] = useState(true);
    const { register, handleSubmit } = useForm();
    const [phone, setPhone] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const navigate = useNavigate();
    const [getAccount, setGetAccount] = useState();
    const [isRepeatEmail, setIsRepeatEmail] = useState(true);
    const [compare, setCompare] = useState(true);
    const handleOTP = () => {
        window.recaptchaVerifier = new RecaptchaVerifier(
            'sign-in-button',
            {
                size: 'invisible',
                callback: (response) => {},
            },
            firebaseRe,
        );
    };
    const handleSignin = (e) => {
        const data = {
            name: e.FullName,
            email: e.email,
            password: window.btoa(e.passWord),
            isUser: e.gender,
            number: e.number,
        };
        // setDataUser(data);

        const isRepeat = getAccount.some((value) => {
            return value.email === e.email;
        });

        if (isRepeat) {
            setIsRepeatEmail(false);
        } else if (e.passWord !== e.RepassWord) {
            setIsRepeatEmail(true);
            setCompare(false);
        } else {
            setIsModalOpen(true);
            // OTP
            handleOTP();

            const number = '+84' + e.number.slice(1, e.number.length);
            let appVerifire = window.recaptchaVerifier;
            signInWithPhoneNumber(firebaseRe, number, appVerifire)
                .then((confirmationResult) => {
                    window.confirmationResult = confirmationResult;
                    setTestPhone(true);
                })
                .catch((error) => {
                    console.log(error);
                    setTestPhone(false);
                });
            //OTP
            if (phone) {
                const isLogin = axios.post('https://63e25f35ad0093bf29ce6905.mockapi.io/users', { ...data });
                if (isLogin) {
                    toast.success('Tạo thành công');
                    navigate('/sign-up');
                } else {
                    toast.error('Tạo tài khoản thất bại ');
                }
            }
        }
    };

    const handleCheckRepeat = async () => {
        const getListAccount = await axios({
            method: 'GET',
            url: 'https://63e25f35ad0093bf29ce6905.mockapi.io/users',
        });
        setGetAccount(getListAccount.data);
    };
    useEffect(() => {
        handleCheckRepeat();
    }, []);
    useEffect(() => {
        if (OTP.length === 6) {
            const newNumber = parseInt(OTP);
            window.confirmationResult
                .confirm(newNumber)
                .then(async (res) => {
                    setPhone(true);
                    setIsModalOpen(false);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }, [OTP]);
    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };
    return (
        <div className="w-full h-[100vh] ">
            <Link to="/" className="border-solid border-[1px] border-b-[#b9b9b951] fixed w-full bg-white">
                <LogoLogin />
            </Link>
            <div id="sign-in-button"></div>
            <div className="h-full  bg-[#00b14f]">
                <Grid className="place-items-center grid h-full">
                    <Card style={{ maxWidth: 450, padding: '20px 5px', margin: '0 auto' }}>
                        <CardContent>
                            <Typography gutterBottom variant="h5">
                                <h5 className="font-semibold text-3xl"> Sign Up</h5>
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                                <p className="my-3"> Create your accont in here !</p>
                            </Typography>
                            <form onSubmit={handleSubmit(handleSignin)}>
                                <Grid container spacing={1}>
                                    <Grid xs={12} sm={8} item>
                                        <TextField
                                            placeholder="Enter first name"
                                            label="Full Name"
                                            variant="outlined"
                                            fullWidth
                                            color="success"
                                            required
                                            {...register('FullName')}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={4}>
                                        <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label" color="success">
                                                Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                // value={age}
                                                label="Age"
                                                // onChange={handleChange}
                                                color="success"
                                                {...register('gender')}
                                            >
                                                <MenuItem value={'Client'}>Client</MenuItem>
                                                <MenuItem value={'Saler'}>Saler</MenuItem>
                                                <MenuItem value={'Delivery'}>Delivery</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        {isRepeatEmail ? (
                                            <TextField
                                                type="email"
                                                placeholder="Enter email"
                                                label="Email"
                                                variant="outlined"
                                                fullWidth
                                                required
                                                color="success"
                                                {...register('email')}
                                            />
                                        ) : (
                                            <TextField
                                                type="email"
                                                placeholder="Enter email"
                                                label="Email"
                                                variant="outlined"
                                                fullWidth
                                                required
                                                color="success"
                                                {...register('email')}
                                                error
                                                helperText="Email đã tồn tại"
                                            />
                                        )}
                                    </Grid>
                                    <Grid item xs={12}>
                                        {testPhone ? (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Number"
                                                type="number"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                {...register('number')}
                                            />
                                        ) : (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Number"
                                                type="number"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                {...register('number')}
                                                error
                                                helperText="Number không chính xác"
                                            />
                                        )}
                                    </Grid>

                                    <Grid item xs={12}>
                                        <TextField
                                            id="outlined-password-input"
                                            label="Password"
                                            type="password"
                                            autoComplete="current-password"
                                            required
                                            color="success"
                                            fullWidth
                                            {...register('passWord')}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        {compare ? (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Enter the password  "
                                                type="password"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                {...register('RepassWord')}
                                            />
                                        ) : (
                                            <TextField
                                                id="outlined-password-input"
                                                label="Enter the password  "
                                                type="password"
                                                autoComplete="current-password"
                                                required
                                                color="success"
                                                fullWidth
                                                error
                                                helperText="Password nhập lại không dúng"
                                                {...register('RepassWord')}
                                            />
                                        )}
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Button
                                            type="submit"
                                            variant="contained"
                                            color="success"
                                            fullWidth
                                            className="rounded-3xl h-12"
                                            // onClick={showModal}
                                        >
                                            Sign In
                                        </Button>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Link
                                            to="/sign-up"
                                            className="cursor-pointer text-[#00b14f] no-underline hover:underline mr-3 flex items-center mt-2"
                                        >
                                            <Icon icon="mdi:arrow-left" className="mr-1" /> Back to Login
                                        </Link>
                                    </Grid>
                                </Grid>
                            </form>
                        </CardContent>
                    </Card>
                </Grid>
            </div>
            <Modal
                title="Basic Modal"
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                cancelButtonProps={{ style: { display: 'none' } }}
                okButtonProps={{ style: { display: 'none' } }}
            >
                <OTPInput
                    value={OTP}
                    onChange={setOTP}
                    autoFocus
                    OTPLength={6}
                    otpType="number"
                    disabled={false}
                    className="focus:border-[#d2d2d2] h-14"
                    inputClassName=" rounded border-solid border-[1px] border-[#00b14f] h-14"
                />
                <ResendOTP onResendClick={() => window.location.reload()} />
            </Modal>
        </div>
    );
}

export default SignIn;
