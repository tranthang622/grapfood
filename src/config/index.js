import URL, { BASEURL } from './baseURL';
import routes from './routes';
const config = {
    URL,
    BASEURL,
    routes,
};
export default config;
