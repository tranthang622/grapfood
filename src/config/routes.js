const routes = {
    home: '/',
    signUp: 'sign-up',
    signIn: '/sign-in',
    restaurant: '/restaurant',
    restaurant_food: '/restaurant/food',
    checkout: '/checkout',
};
export default routes;
