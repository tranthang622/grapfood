import ContentCPN from '../../components/ContentCPN/ContentCPN';

function Body() {
    return (
        <div className=" z-0 w-full h-full ">
            <ContentCPN />
        </div>
    );
}

export default Body;
