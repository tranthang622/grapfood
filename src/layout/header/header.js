import HeaderCPN from '../../components/HeaderCPN/HeaderCPN';
import { useEffect, useState } from 'react';
function Header() {
    const [stickyClass, setStickyClass] = useState('bg-transparent ');

    useEffect(() => {
        window.addEventListener('scroll', stickNavbar);

        return () => {
            window.removeEventListener('scroll', stickNavbar);
        };
    }, []);

    const stickNavbar = () => {
        if (window !== undefined) {
            let windowHeight = window.scrollY;
            windowHeight > 2
                ? setStickyClass(' bg-white ease-in-out delay-150 shadow-md ')
                : setStickyClass('bg-transparent');
        }
    };
    const geWidth = window.innerWidth;
    return (
        <div
            className={`w-full lg:h-24 h-16  transition-all ease-in-out delay-150 fixed top-0 left-0 z-50 ${stickyClass} `}
        >
            <HeaderCPN />
        </div>
    );
}

export default Header;
