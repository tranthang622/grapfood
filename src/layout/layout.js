import { Col, Container, Row } from 'react-bootstrap';
import Body from './body/body';
import Footer from './footer/footer';
import Header from './header/header';
function Layout({ children }) {
    return (
        <div className="w-full h-full">
            <Header />

            {/* <Body /> */}

            {children}
            <Footer />
        </div>
    );
}

export default Layout;
