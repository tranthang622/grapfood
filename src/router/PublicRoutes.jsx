import React, { Fragment, useEffect } from 'react';

//router
import { Route, Routes, useNavigate } from 'react-router-dom';
import { privateRoutes, publicRouter } from '../routes';

//layout
// import DefaultLayout from '../layouts/DefaultLayout';
import DefaultLayout from '../layout/layout';
const PublicRoutes = () => {
    return (
        <Routes>
            {publicRouter?.map((route, i) => {
                let Layout = DefaultLayout;
                if (route?.layout) Layout = route?.layout;
                else if (route?.layout === null) Layout = Fragment;
                const Page = route?.component;
                return (
                    <Route
                        key={i}
                        path={route?.path}
                        element={
                            <Layout>
                                {' '}
                                <Page />{' '}
                            </Layout>
                        }
                        children={
                            route?.children
                                ? route?.children.map((child, i) => {
                                      const PageChild = child.component;
                                      return <Route key={i} path={child.path} element={<PageChild />} />;
                                  })
                                : null
                        }
                    />
                );
            })}
        </Routes>
    );
};

export default PublicRoutes;
