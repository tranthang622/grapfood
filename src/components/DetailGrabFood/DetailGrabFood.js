import { Container } from 'react-bootstrap';

import { Swiper, SwiperSlide } from 'swiper/react';
import DirectoryFood from './component/DirectoryFood';
import InfoPlaceBuy from './component/InfoPlaceBuy';
import 'swiper/css';
import 'swiper/css/pagination';
import t from '../../utilities/i18n';
// import required modules

import 'swiper/css';
import 'swiper/css/pagination';
// import required modules
import { Keyboard, Mousewheel, Navigation, Pagination } from 'swiper';
function DetailGrabFood() {
    return (
        <div className="h-full ">
            <Container fluid="md" className=" ">
                <InfoPlaceBuy />
            </Container>
            <div className="h-16  w-full sticky position_Sticky z-30 top-24 shadow-md bg-white ">
                <Swiper
                    slidesPerView={6}
                    spaceBetween={0}
                    pagination={{
                        clickable: true,
                    }}
                    modules={[Pagination]}
                    className="h-16 cursor-pointer"
                >
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0 border-b-[#00b14f] border-b-[3px] h-[60%] px-2 text-base text-[#545454]">
                                Món chiên xào
                            </p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                    <SwiperSlide>
                        <center className=" h-full w-[80%] flex items-center justify-center text-center ">
                            <p className="mb-0   h-[60%] px-2 text-base text-[#545454]">Món chiên xào</p>
                        </center>
                    </SwiperSlide>
                </Swiper>
            </div>
            <DirectoryFood />
        </div>
    );
}

export default DetailGrabFood;
