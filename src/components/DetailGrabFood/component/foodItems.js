import { Icon } from '@iconify/react';
import Food1 from '../../../assets/typeOfFood/Food1.webp';
function FoodItems({ foodName, gift, price, img }) {
    return (
        <div className="h-44  bg-white rounded-lg mb-4 flex items-center px-3 relative hover:border-solid hover:border-[1px] cursor-pointer hover:border-[#00b14f] ">
            <div>
                <img src={img} alt="" className="w-32 h-32 mr-4 " />
            </div>
            <div className="">
                <p className="">{foodName}</p>
                <p className="text-sm  text-[#9c9c9c] mb-7">{gift}</p>
                <p className="text-base font-semibold">
                    {new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(price)}
                </p>
                <Icon icon="mdi:plus-box" className="text-[#00b14f] text-4xl absolute bottom-3 right-3" />
            </div>
        </div>
    );
}

export default FoodItems;
