import { Icon } from '@iconify/react';
import { FormControl, MenuItem } from '@mui/material';
import { ConfigProvider, DatePicker, Select } from 'antd';
import { Link } from 'react-router-dom';
import t from '../../../utilities/i18n';

function InfoPlaceBuy() {
    return (
        <div className="mt-36 mb-11 ">
            <div className=" flex">
                <Link to="/" className="text-[#00a5cf] text-lg hover:text-[#00b14f] cursor-pointer no-underline">
                    Trang chủ
                </Link>
                <Icon icon="material-symbols:chevron-right-rounded" className="text-3xl text-[#999999] font-thin" />

                <Link
                    to="/restaurant/food"
                    className="text-[#00a5cf] text-lg hover:text-[#00b14f] cursor-pointer no-underline"
                >
                    Nhà hàng
                </Link>
                <Icon icon="material-symbols:chevron-right-rounded" className="text-3xl text-[#999999] font-thin" />
                <p>Dakgalbi - Tiệm gà rán Hàn Quốc - Nguyễn Công Hoan</p>
            </div>
            <h1 className="font-semibold tracking-tight">Dakgalbi - Tiệm gà rán Hàn Quốc - Nguyễn Công Hoan</h1>
            <p className="text-base text-[#757575]">Gà Rán - Burger</p>
            <div className="flex text-[#676767]">
                <div className="flex mr-8">
                    <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                    <p> 4.7</p>
                </div>
                <div className="flex mr-8 ">
                    <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                    <p> 15 {t('mins')}</p>
                </div>
                <div>1.7 km</div>
            </div>
            <div className="flex">
                <p className="text-[#676767] font-semibold text-base mr-5">Giờ mở cửa</p>
                <p className="text-[#676767] text-base">Hôm nay 10:00-15:00 17:00-21:20</p>
            </div>
            <div className="flex items-center">
                <Icon icon="fluent:tag-28-regular" className="text-[#00b14f] text-2xl mr-1" />
                <p className="mb-0 text-sm">Bánh gạo lắc phô mai Giảm 7.000₫</p>
            </div>
            <div className="flex items-center">
                <Icon icon="fluent:tag-28-regular" className="text-[#00b14f] text-2xl mr-1" />
                <p className="mb-0 text-sm mr-36">Ưu đãi đến 60K</p>

                <p className="mb-0 text-base font-semibold text-[#00a5cf] cursor-pointer">Xem chi tiết</p>
            </div>
            {/* <RangePicker showTime /> */}
            {/* <Select
                defaultValue="VI"
                style={{ width: 70 }}
                // onChange={handleChange}
                className="flex items-center ml-2 px-1 h-10 shadow-[0 2px 0 rgb(0 0 0 / 2%)] border-[1px] border-[#d9d9d9] bg-white text-gray-600 rounded-md"
                options={[
                    {
                        value: 'VI',
                        label: 'VI',

                        // style: ' height: 70',
                    },
                    {
                        value: 'EN',
                        label: 'EN',
                    },
                ]}
            /> */}
            <div className="mt-2">
                <FormControl sx={{ m: 1, minWidth: 400 }}>
                    <ConfigProvider
                        theme={{
                            token: {
                                colorPrimary: '#00b96b',
                                controlHeight: 46,
                            },
                        }}
                    >
                        <Select
                            suffixIcon={<Icon icon="ic:baseline-calendar-month" className="text-2xl" />}
                            // menuItemSelectedIcon={<Icon icon="ic:baseline-calendar-month" />}
                            value={'Ngày giao hàng : Hôm nay'}
                            //   onChange={handleChange}
                            // displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            className={'h-12'}
                        >
                            <MenuItem value={10}>
                                <p className={'font-bold mb-0 text-base'}> Hôm nay</p>
                            </MenuItem>
                            <MenuItem value={20}>Sun ,18 Dec</MenuItem>
                            <MenuItem value={30}>Mon ,18 Dec</MenuItem>
                        </Select>
                    </ConfigProvider>
                </FormControl>
                <FormControl sx={{ m: 1, minWidth: 400 }}>
                    <ConfigProvider
                        theme={{
                            token: {
                                colorPrimary: '#00b96b',
                                controlHeight: 46,
                            },
                        }}
                    >
                        <Select
                            suffixIcon={<Icon icon="ph:clock" className="text-2xl" />}
                            value={'Thời gian giao : Ngay bây giờ'}
                            //   onChange={handleChange}
                            // displayEmpty
                            inputProps={{ 'aria-label': 'Without label' }}
                            className={'h-12'}
                        >
                            <MenuItem value={10}>
                                <p className={'font-bold mb-0 text-base'}> Ngay bây giờ</p>
                            </MenuItem>
                            {/* <MenuItem value={20}>Sun ,18 Dec</MenuItem>
                            <MenuItem value={30}>Mon ,18 Dec</MenuItem> */}
                        </Select>
                    </ConfigProvider>
                </FormControl>
            </div>
        </div>
    );
}

export default InfoPlaceBuy;
