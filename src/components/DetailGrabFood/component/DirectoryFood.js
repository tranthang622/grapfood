import { Col, Container, Row } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Keyboard, Mousewheel, Navigation, Pagination } from 'swiper';
import Food1 from '../../../assets/typeOfFood/Food1.webp';
import 'swiper/css';
import 'swiper/css/pagination';
// import required modules
import FoodItems from './foodItems';
import { useState } from 'react';
import { Icon } from '@iconify/react';
import { Button, Input } from 'antd';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addFood } from '../../../redux/silceRedux/basketReduce';
import axios from 'axios';
import dataListFood from '../../../database/ListFood.json';
function DirectoryFood() {
    const [showbaket, setShowbaket] = useState(false);
    const [count, setCount] = useState(1);
    const [total, setTotal] = useState();
    const [listFood, setListFood] = useState([]);
    const [passFood, setPassFood] = useState();
    const dispatch = useDispatch();
    const handleShowBacket = (food) => {
        setShowbaket(true);
        setCount(1);
        setPassFood(food);
    };
    const handleClose = () => {
        setShowbaket(false);
    };
    const handlePlus = () => {
        setCount((e) => e + 1);
    };
    const handleSubtr = () => {
        if (count >= 1) {
            setCount((e) => e - 1);
        }
    };

    useEffect(() => {
        setTotal(() => passFood?.price * count);
    });
    const handleBasket = () => {
        dispatch(addFood({ ...passFood, count: count }));
        setShowbaket(false);
    };
    const handleProductAPI = async () => {
        const getValue = await axios({
            method: 'get',
            url: 'https://63d7e1e65c4274b136fda71b.mockapi.io/product/products',
        });
        if (getValue) {
            setListFood(getValue.data);
        }
    };
    useEffect(() => {
        // handleProductAPI();
        setListFood(dataListFood);
    }, []);
    // console.log(dataListFood);
    return (
        <div className="bg-[#f7f7f7] w-full h-full z-10 ">
            <Container fluid="md">
                <h1 className="mb-10 pt-10">Ưu đãi hôm nay</h1>
                <Row>
                    {listFood?.map((food) => (
                        <Col key={food?.foodId} md={4} onClick={() => handleShowBacket(food)}>
                            <FoodItems foodName={food.foodName} gift={food.gift} price={food.price} img={food.img} />
                        </Col>
                    ))}
                </Row>

                <p className="text-center text-sm text-[#747474] mb-0 py-6">
                    Chúng tôi luôn cố gắng cập nhật thông tin chính xác nhất.{' '}
                    <span className="text-[#3b89ff] cursor-pointer">Hãy báo với chúng tôi</span> nếu bạn thấy bất kỳ
                    thông tin không chính xác nào.
                </p>
            </Container>
            <div
                className={`fixed lg:w-[526px] w-full h-full top-0 right-0  bg-white z-50  ${
                    !showbaket
                        ? 'translate-x-full '
                        : `translate-x-0 before:content-[''] before:absolute before:top-0 before:left-[-100vw] before:w-[100vw]  before:h-full before:bg-[#0000001a]`
                }  transition ease-in-out delay-150 duration-300  `}
            >
                <Icon
                    icon="material-symbols:close-rounded"
                    className="text-4xl m-3 text-[#999999]   cursor-pointer"
                    onClick={handleClose}
                />
                <hr className="w-full my-6 h-[.5px] bg-[#b6b6b613]" />
                <div>
                    <div className="flex justify-around">
                        <img src={passFood?.img} alt="" className="w-32 h-32 mr-4 rounded-lg " />
                        <p className="text-xl font-medium">{passFood?.foodName}</p>
                        <p className="text-xl font-bold">
                            {new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(
                                passFood?.price,
                            )}
                        </p>
                    </div>
                    <div className="w-full my-6 h-2 bg-[#f7f7f7]"></div>
                    <div className="px-3">
                        <span className="text-base font-semibold mr-2">Special instructions</span>
                        <span className="text-sm text-[#575757]">Optional</span>
                        <Input
                            placeholder="E.g. No onions please"
                            className="mt-2 h-12 hover:border-[#00b14f] focus:border-[#00b14f]"
                        />
                    </div>
                    <div className="absolute bottom-0 shadow-inner w-full h-24 flex items-center">
                        <div className="px-3 flex items-center justify-evenly w-full">
                            <div
                                onClick={handleSubtr}
                                className=" cursor-pointer text-[#3ea2ff] text-4xl w-9 h-9 border-solid border-[1px] border-[#d4d4d496] rounded-sm leading-6 text-center"
                            >
                                -
                            </div>
                            <h3>{count}</h3>
                            <div
                                onClick={handlePlus}
                                className=" cursor-pointer text-[#3ea2ff] text-3xl w-9 h-9 border-solid border-[1px] border-[#d4d4d496] rounded-sm leading-7 text-center"
                            >
                                +
                            </div>
                            {count > 0 ? (
                                <Button
                                    className="bg-[#00b14f] text-white w-80 h-10  text-base font-semibold"
                                    onClick={handleBasket}
                                >
                                    Add to Basket -{' '}
                                    {new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(
                                        total,
                                    )}
                                </Button>
                            ) : (
                                <Button
                                    className="bg-[#ee6352] text-white w-80 h-10  text-base font-semibold"
                                    onClick={handleClose}
                                >
                                    Cancel
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DirectoryFood;
