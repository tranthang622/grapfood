import { Container } from 'react-bootstrap';
import DirectoryFood from './components/FoodOfRestaurants';
import FindFood from './components/findFood';

function DetailRestaurants() {
    return (
        <div className=" w-full mt-24 ">
            <FindFood />
            <div className="h-2 bg-[#f7f7f7] w-full mb-5"></div>
            <DirectoryFood />
        </div>
    );
}

export default DetailRestaurants;
