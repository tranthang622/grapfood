import { Icon } from '@iconify/react';
import { Col, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import food1 from '../../../assets/background/food1.jpg';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import t from '../../../utilities/i18n';
// import required modules
import { Keyboard, Mousewheel, Navigation, Pagination } from 'swiper';
function DirectoryFood() {
    return (
        <Container fluid="md">
            <div className=" flex">
                <Link to="/" className="text-[#00a5cf] text-lg hover:text-[#00b14f] cursor-pointer no-underline">
                    Trang chủ{' '}
                </Link>
                <Icon icon="material-symbols:chevron-right-rounded" className="text-3xl text-[#999999] font-thin" />

                <Link
                    to="/restaurant/food"
                    className="text-[#00a5cf] text-lg hover:text-[#00b14f] cursor-pointer no-underline"
                >
                    Nhà hàng
                </Link>
            </div>
            <h1 className="font-semibold tracking-tight mb-5">Món ngon gần bạn</h1>
            <Swiper
                slidesPerView={4}
                spaceBetween={30}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination]}
                className="h-80 cursor-pointer"
            >
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
            </Swiper>
            <div className="flex text-4xl font-medium my-5">
                <span className="mr-1">Các quán ăn tại</span>
                <span className="text-[#00b14f]">54 LIễu Giai, P.Ngọc Khánh, Q.Ba Đình, Hà Nội, 10000,...</span>
            </div>
            <Row>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>{' '}
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
                <Col md={3} className="mb-5 ">
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline mb-3 ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-lg "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}

export default DirectoryFood;
