import { Icon } from '@iconify/react';
import { ConfigProvider, Input } from 'antd';
import { Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import t from '../../../utilities/i18n';
// import required modules
import { Keyboard, Mousewheel, Navigation, Pagination } from 'swiper';
import food2 from '../../../assets/typeOfFood/food2.jpg';

import { Link } from 'react-router-dom';
function FindFood() {
    return (
        <div>
            <Container fluid="md">
                <ConfigProvider
                    theme={{
                        token: {
                            colorPrimary: '#00b96b',
                            controlHeight: 46,
                        },
                    }}
                >
                    <Input
                        className="h-11 mb-3"
                        placeholder="Tìm món hoặc quán ăn ăn"
                        prefix={
                            <Icon icon="material-symbols:search-rounded" className="mr-1 text-2xl text-[#515151]" />
                        }
                        // suffix={<Icon icon="ic:twotone-gps-fixed" color="#00b14f" />}
                    />
                </ConfigProvider>

                <Swiper
                    slidesPerView={6}
                    spaceBetween={30}
                    pagination={{
                        clickable: true,
                    }}
                    modules={[Pagination]}
                    className="h-40 cursor-pointer"
                >
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 text-shadown_edit bg-cover bg-center overflow-hidden relative mb-3 rounded-xl after:content-['Gần_tôi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                    <SwiperSlide>
                        <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                            <div
                                className="w-full h-28 bg-cover bg-center text-shadown_edit overflow-hidden relative mb-3 rounded-xl after:content-['Khuyến_Mãi'] after:absolute after:w-full after:h-full text-white after:flex after:items-center after:justify-center after:bg-[#00000056]"
                                style={{ backgroundImage: `url(${food2})` }}
                            ></div>
                        </Link>
                    </SwiperSlide>
                </Swiper>
            </Container>
        </div>
    );
}

export default FindFood;
