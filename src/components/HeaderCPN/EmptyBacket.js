import { Icon } from '@iconify/react';

function EmptyBacket({ handleClosed }) {
    return (
        <div>
            <Icon
                icon="material-symbols:close-rounded"
                className="text-4xl m-3 text-[#999999]  cursor-pointer"
                onClick={handleClosed}
            />
            <div className="flex flex-col justify-center h-full">
                <div className="flex justify-center">
                    <img src="https://food.grab.com/static/images/ilus-basket-empty.svg" alt="" className="w-80 " />
                </div>

                <p className="text-lg text-center font-bold">Start Grabbing Food!</p>
                <p className="text-xs text-center text-[#999999]">Add items to your basket and place order here.</p>
                <p className="font-medium text-center text-[#00a5cf] cursor-pointer" onClick={handleClosed}>
                    Continue browser
                </p>
            </div>
        </div>
    );
}

export default EmptyBacket;
