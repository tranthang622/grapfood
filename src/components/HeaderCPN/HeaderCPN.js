import { Icon } from '@iconify/react';
import { Button, ConfigProvider, Select } from 'antd';
import { Col, Container, Row } from 'react-bootstrap';
import { useState } from 'react';
import { useEffect } from 'react';
import t from '../../utilities/i18n';
import i18n from '../../i18n';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { basketSelector } from '../../redux/selector';
import { LogoGrapFood } from '../../assets/svg/logoSVG';
import EmptyBacket from './EmptyBacket';
import FullBacket from './FullBacket';
import axios from 'axios';
function HeaderCPN() {
    const [showbaket, setShowbaket] = useState(false);
    const [showProfile, setShowProfile] = useState(false);
    const [handeCount, setHandleCount] = useState([]);
    const pathLogout = useNavigate();
    const handleShowBacket = () => {
        setShowbaket(true);
    };
    const handleClose = () => {
        setShowbaket(false);
    };
    const handleCloseUser = () => {
        setShowProfile(false);
    };

    const handlePlus = (value_id) => {
        setHandleCount((e) =>
            e.map((value) => (value.foodId === value_id ? { ...value, count: value.count + 1 } : value)),
        );
    };
    const handleSubtr = (value_id) => {
        setHandleCount((e) =>
            e.map((value) =>
                value.foodId === value_id ? { ...value, count: value.count - (value.count > 0 ? 1 : 0) } : value,
            ),
        );
    };
    const handleChange = (value) => {
        if (value === 'VI') {
            i18n.changeLanguage('vi');
        }
        if (value === 'EN') {
            i18n.changeLanguage('en');
        }
    };

    const listBasket = useSelector(basketSelector);
    useEffect(() => {
        setHandleCount(listBasket.totalBasket);
    }, [listBasket]);

    const handleRemove = (value_id) => {
        setHandleCount((e) => e.filter((value) => value.foodId !== value_id));
    };
    const gatParse = JSON.parse(localStorage.getItem('GrabAccount'));
    const handleShowProfile = () => {
        setShowProfile(true);
    };
    const hadleLogOut = () => {
        localStorage.removeItem('GrabAccount');
        pathLogout('/sign-up');
    };
    const geWidth = window.innerWidth;
    return (
        <Container fluid="md">
            <Row>
                <Col>
                    {' '}
                    <div className={`flex justify-between  items-center lg:h-24 h-20`}>
                        <Link to="/">
                            <LogoGrapFood setSVG={geWidth} />
                        </Link>
                        <div className="flex">
                            <ConfigProvider
                                theme={{
                                    token: {
                                        colorPrimary: '#00b14f',
                                    },
                                }}
                            >
                                <Button
                                    className={`flex relative justify-center items-center text-xs  lg:h-10 font-semibold ${
                                        handeCount.length > 0
                                            ? 'bg-[#00b14f]  text-white w-20'
                                            : ' bg-white text-gray-600'
                                    } `}
                                    icon={
                                        <Icon
                                            icon="simple-line-icons:handbag"
                                            className={`${handeCount.length > 0 ? 'mr-2' : ''}`}
                                        />
                                    }
                                    onClick={handleShowBacket}
                                >
                                    {handeCount.length > 0
                                        ? new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(
                                              handeCount.reduce(
                                                  (init, curentvalue) => curentvalue.count * curentvalue.price + init,
                                                  0,
                                              ),
                                          )
                                        : ''}
                                    {handeCount.length > 0 ? (
                                        <div className="absolute  top-[-10px] left-[-14px] min-w-[20px] h-5 rounded-[50%] border-solid border-[1px] border-[#00b14f] bg-white text-xs text-center text-[#00b14f]">
                                            {handeCount.length}
                                        </div>
                                    ) : (
                                        ''
                                    )}
                                </Button>
                            </ConfigProvider>

                            {gatParse ? (
                                <ConfigProvider
                                    theme={{
                                        token: {
                                            colorPrimary: '#00b14f',
                                        },
                                    }}
                                >
                                    <Button
                                        className="ml-2 font-semibold lg:h-10 text-xs lg:text-sm bg-white text-gray-600"
                                        onClick={handleShowProfile}
                                    >
                                        {gatParse?.name !== '' ? gatParse.name : t('Sign-up') / t('sign-in')}
                                    </Button>
                                </ConfigProvider>
                            ) : (
                                <Link to="/sign-up">
                                    <ConfigProvider
                                        theme={{
                                            token: {
                                                colorPrimary: '#00b14f',
                                            },
                                        }}
                                    >
                                        <Button className="ml-2 font-semibold lg:h-10 text-xs lg:text-sm bg-white text-gray-600">
                                            {t('Sign-up')} / {t('sign-in')}
                                        </Button>
                                    </ConfigProvider>
                                </Link>
                            )}
                            <ConfigProvider
                                theme={{
                                    token: {
                                        colorPrimary: '#00b96b',
                                        controlHeight: geWidth <= 900 ? 32 : 40,
                                        fontSize: geWidth <= 900 ? 12 : 14,
                                    },
                                }}
                            >
                                <Select
                                    defaultValue="VI"
                                    style={{ width: 70 }}
                                    onChange={handleChange}
                                    className={`flex items-center text-xs lg:ml-2 lg:px-1 ${
                                        geWidth <= 900 && 'px-2'
                                    }   shadow-[0 2px 0 rgb(0 0 0 / 2%)]   text-gray-600 rounded-md`}
                                    options={[
                                        {
                                            value: 'VI',
                                            label: 'VI',
                                        },
                                        {
                                            value: 'EN',
                                            label: 'EN',
                                        },
                                    ]}
                                />
                            </ConfigProvider>
                        </div>
                    </div>
                </Col>
            </Row>
            <div
                className={`fixed lg:w-[526px] w-full h-full top-0 right-0  bg-white   ${
                    !showbaket
                        ? 'translate-x-full '
                        : `translate-x-0 before:content-[''] before:absolute before:top-0 before:left-[-100vw] before:w-[100vw]  before:h-full before:bg-[#0000001a]`
                }  transition ease-in-out delay-150 duration-300  `}
            >
                {listBasket.totalBasket.length <= 0 ? (
                    <EmptyBacket handleClosed={handleClose} />
                ) : (
                    <FullBacket
                        handeCountSub={handeCount}
                        handleSubtrSub={handleSubtr}
                        handleCloseSub={handleClose}
                        handleRemoveSub={handleRemove}
                        handlePlusSub={handlePlus}
                    />
                )}
            </div>
            <div
                className={`fixed  lg:w-[526px] w-full h-full   top-0 right-0  bg-white   ${
                    !showProfile
                        ? 'translate-x-full '
                        : `translate-x-0 before:content-[''] before:absolute before:top-0 before:left-[-100vw] before:w-[100vw]  before:h-full before:bg-[#0000001a]`
                }  transition ease-in-out delay-150 duration-300  `}
            >
                <Icon
                    icon="material-symbols:close-rounded"
                    className="text-4xl m-3 text-[#999999] z-10 relative float-left cursor-pointer"
                    onClick={handleCloseUser}
                />
                <hr className="w-full my-6 h-[.5px] bg-[#b6b6b613]" />
                <div className="cursor-pointer  m-4 text-sm">Đơn hàng hiện có</div>
                <div onClick={hadleLogOut} className="cursor-pointer  m-4 text-sm">
                    Logout
                </div>
            </div>
        </Container>
    );
}

export default HeaderCPN;
