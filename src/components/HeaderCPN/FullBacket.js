import { Icon } from '@iconify/react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';

function FullBacket({ handeCountSub, handleSubtrSub, handleCloseSub, handleRemoveSub, handlePlusSub }) {
    const gatParse = JSON.parse(localStorage.getItem('GrabAccount'));
    return (
        <div>
            <Icon
                icon="material-symbols:close-rounded"
                className="text-4xl m-3 text-[#999999] z-10 relative float-left cursor-pointer"
                onClick={handleCloseSub}
            />
            <div className="relative top-3 z-0">
                <p className="font-semibold text-center text-base mb-0">Giỏ đồ ăn</p>
                <p className="flex justify-center text-[#787878] text-xs">
                    <Icon icon="ph:clock" className="mt-[2px] mb-1" /> Thời gian giao: 20 phút (Cách bạn 1Sub,8 km)
                </p>
            </div>
            <hr className="w-full my-6 h-[.5px] bg-[#b6b6b613]" />
            <div>
                <h5 className="m-3 font-medium">Bún Bò Huế An Cựu - Đội Cấn</h5>
                <div className="overflow-y-scroll h-[32rem]">
                    <div className=" ">
                        {handeCountSub.map((value) => (
                            <div key={value?.foodName}>
                                <div className="flex justify-between items-center px-4">
                                    <div className="flex items-center">
                                        <div className="flex items-center mr-3">
                                            <div
                                                onClick={() => {
                                                    handleSubtrSub(value?.foodId);
                                                }}
                                                className=" cursor-pointer text-[#3ea2ff] text-3xl mr-3 mb-1  "
                                            >
                                                -
                                            </div>
                                            <p className=" mr-3 mt-3">{value?.count}</p>
                                            <div
                                                onClick={() => {
                                                    handlePlusSub(value?.foodId);
                                                }}
                                                className="  cursor-pointer text-[#3ea2ff] text-2xl mb-1  "
                                            >
                                                +
                                            </div>
                                        </div>
                                        <img src={value?.img} alt="" className="w-14 h-14 mr-4 rounded-lg " />
                                        <p className="text-sm font-medium mt-2">{value?.foodName}</p>
                                    </div>

                                    {value?.count > 0 ? (
                                        <p className="text-base">
                                            {new Intl.NumberFormat('de-DE', {
                                                style: 'currency',
                                                currency: 'VND',
                                            }).format(value?.price * value?.count)}
                                        </p>
                                    ) : (
                                        <p
                                            className="font-bold text-[#ee6352] cursor-pointer hover:text-[#eead52]"
                                            onClick={() => handleRemoveSub(value?.foodId)}
                                        >
                                            {' '}
                                            Remove
                                        </p>
                                    )}
                                </div>
                                <hr className="w-[95%] flex justify-center my-6 h-[.5px] ml-4 bg-[#b6b6b613]" />
                            </div>
                        ))}
                    </div>

                    <div className="px-3">
                        <div className="flex justify-between">
                            <span className="text-sm ">Tổng</span>
                            <span>
                                {new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(
                                    handeCountSub.reduce(
                                        (init, curentvalue) => curentvalue.count * curentvalue.price + init,
                                        0,
                                    ),
                                )}
                            </span>
                        </div>
                        <p className="text-sm">Delivery Fee will be shown after you review order</p>
                    </div>
                </div>
                <div className="absolute bottom-0 shadow-inner w-full h-28 ">
                    <div className="flex justify-between px-3 py-2 ">
                        <span className="text-xl ">Tổng Cộng</span>
                        <span>
                            {new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(
                                handeCountSub.reduce(
                                    (init, curentvalue) => curentvalue.count * curentvalue.price + init,
                                    0,
                                ),
                            )}
                        </span>
                    </div>
                    <div className="px-3 flex items-center justify-evenly w-full">
                        {gatParse ? (
                            <Link to={'/checkout'}>
                                <Button className="bg-[#00b14f] text-white w-96 h-12  text-lg font-semibold">
                                    Xem lại đơn hàng
                                </Button>
                            </Link>
                        ) : (
                            <Link to={'/sign-up'}>
                                <Button className="bg-[#00b14f] text-white w-96 h-12  text-lg font-semibold">
                                    Đăng nhập để đặt đơn
                                </Button>
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FullBacket;
