import { Col, Container, Image, Row } from 'react-bootstrap';
import bgcontent from '../../assets/background/bg1.jpg';
import IntroApp from './introApp';
import IntroFood from './IntroFood';
import ListFood from './ListFood';
import SearchPosition from './SearchPosition';
import TypesFood from './TypesFood';
function ContentCPN() {
    const geWidth = window.innerWidth;
    return (
        <div>
            <div
                className="h-96 w-full overflow-hidden background bg-cover bg-center "
                style={{ backgroundImage: `url(${bgcontent})` }}
            ></div>
            <Container fluid="md" className="relative">
                <SearchPosition />
            </Container>
            <hr className="h-[1px] bg-black my-24 z-0" />

            <Container fluid="md" className="relative">
                <ListFood />
                <TypesFood />
                <IntroFood />
            </Container>
            <IntroApp />
        </div>
    );
}

export default ContentCPN;
