import { Button, Input } from 'antd';
import { Icon } from '@iconify/react';
import t from '../../utilities/i18n';
function SearchPosition() {
    const renderTextInDay = () => {
        const hour = new Date().getHours();
        if (hour >= 0 && hour <= 12) return t('Good-Morning');
        if (hour >= 13 && hour <= 18) return t('Good-Afternoon');
        return t('Good-Night');
    };
    return (
        <div className="bg-white w-[360px]  min-h-[388px] rounded-lg absolute top-[-16rem] z-10 border-solid border-[.5px] border-[#e5e9f0]">
            <div className="pt-12 px-4">
                <div className="mb-4">
                    <div className="text-lg font-medium">{renderTextInDay()}</div>
                    <div className="text-4xl font-medium">
                        {t('explore-food')} <br /> {t('good-food')}.
                    </div>
                </div>
                <Input
                    className="h-11 mb-3"
                    placeholder="Nhập địa chỉ của bạn"
                    prefix={<Icon icon="heroicons:map-pin-20-solid" color="#ee6352" />}
                    suffix={<Icon icon="ic:twotone-gps-fixed" color="#00b14f" />}
                />
                <Button className="bg-[#00b14f] text-white w-full h-11 text-lg font-medium">Tìm kiếm</Button>
            </div>
        </div>
    );
}

export default SearchPosition;
