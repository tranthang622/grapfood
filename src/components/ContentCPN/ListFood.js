import { Swiper, SwiperSlide } from 'swiper/react';
import food1 from '../../assets/background/food1.jpg';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import t from '../../utilities/i18n';
// import required modules
import { Icon } from '@iconify/react';
import { Button, Space } from 'antd';
import { Link } from 'react-router-dom';
import { Pagination } from 'swiper';
function ListFood() {
    const geWidth = window.innerWidth;
    return (
        <div>
            <div className=" lg:text-4xl text-xl  font-medium mb-5 overflow-x-hidden w-full">
                {/* <span className="mr-1"></span> */}
                {t('GrabFood-Promo')}
                <span className="text-[#00b14f] ml-1">54, Liễu Giai, P.Ngọc Khánh, Q.Ba Đình, Hà Nội</span>
            </div>
            <Swiper
                slidesPerView={geWidth <= 900 ? 1.4 : 4}
                spaceBetween={30}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination]}
                className="h-80 cursor-pointer"
            >
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
                <SwiperSlide>
                    <Link to="/restaurant" className=" w-full overflow-hidden rounded-xl h-full no-underline ">
                        <div
                            className="w-full h-40 bg-cover bg-center overflow-hidden mb-3 rounded-t-md "
                            style={{ backgroundImage: `url(${food1})` }}
                        ></div>
                        <span className="text-lg font-bold text-[#000] ">Cơm gà Nam An - Láng Hạ</span>
                        <p className="text-[#676767]">Cơm</p>
                        <div className="flex text-[#676767]">
                            <div className="flex mr-3">
                                <Icon icon="material-symbols:star-rate-rounded" className="text-yellow-400 text-2xl" />
                                <p> 4.7</p>
                            </div>
                            <div className="flex mr-3 ">
                                <Icon icon="mdi:clock-time-three-outline" className="text-xl mt-1" />
                                <p> 15 {t('mins')}</p>
                            </div>
                            <div>1.7 km</div>
                        </div>
                        <div className="flex text-[#676767]">
                            <Icon icon="teenyicons:tag-outline" color="#00b14f" className="mr-3 text-xl mt-1" />
                            <p>Combo Breakfast x GrabFood</p>
                        </div>
                    </Link>
                </SwiperSlide>
            </Swiper>
            <Space direction="vertical" style={{ width: '100%' }} className="mt-3 ">
                <Button className="h-11 text-xl font-medium" block>
                    {t('see-all-promotions')}
                </Button>
            </Space>
        </div>
    );
}

export default ListFood;
