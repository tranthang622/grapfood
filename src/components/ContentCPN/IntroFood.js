import { Icon } from '@iconify/react';
import { Button } from 'antd';
import ShowMoreText from 'react-show-more-text';
import t from '../../utilities/i18n';
function IntroFood() {
    return (
        <>
            <div>
                <div className="text-4xl font-medium mb-8">{t('Why-GrabFood')}</div>
                <ul className="p-0 m-0">
                    <li className=" flex mb-4 ">
                        <Icon icon="mdi:tick" color="#00b14f" className="mt-1 mr-1" />
                        <div>
                            <span className="font-bold mr-1">{t('Quickest')}</span>
                            <span>- {t('sub-Quickest')}</span>
                        </div>
                    </li>
                    <li className="flex mb-4 ">
                        <Icon icon="mdi:tick" color="#00b14f" className="mt-1 mr-1 text-xl" />
                        <div>
                            <span className="font-bold  mr-1 ">Dễ dàng nhất</span>
                            <span>
                                - Giờ đây, bạn chỉ cần thực hiện vài cú nhấp chuột hoặc chạm nhẹ là đã có thể đặt đồ ăn.
                                Hãy đặt đồ ăn trực tuyến hoặc tải xuống siêu ứng dụng Grab của chúng tôi để có trải
                                nghiệm nhanh hơn và thú vị hơn.
                            </span>
                        </div>
                    </li>
                    <li className="flex mb-4 ">
                        <Icon icon="mdi:tick" color="#00b14f" className="mt-1 mr-1" />
                        <div>
                            <span className="font-bold mr-1">Đáp ứng mọi nhu cầu</span>
                            <span>
                                - Từ món ăn đặc sản địa phương đến các nhà hàng được ưa thích, nhiều lựa chọn đa dạng
                                chắc chắn sẽ luôn làm hài lòng khẩu vị của bạn.
                            </span>
                        </div>
                    </li>
                    <li className="flex mb-4 ">
                        <Icon icon="mdi:tick" color="#00b14f" className="mt-1 mr-1" />
                        <div>
                            <span className="font-bold mr-1">Thanh toán dễ dàng</span>
                            <span>
                                - Giao và nhận đồ ăn thật dễ dàng. Thanh toán bằng GrabPay thậm chí còn dễ dàng hơn nữa.
                            </span>
                        </div>
                    </li>
                    <li className="flex mb-4 ">
                        <Icon icon="mdi:tick" color="#00b14f" className="mt-1 mr-1" />
                        <div>
                            <span className="font-bold mr-1">Nhiều quà tặng hơn </span>
                            <span>
                                {' '}
                                - Tích điểm GrabRewards cho mỗi đơn hàng của bạn và sử dụng điểm thưởng để đổi lấy nhiều
                                ưu đãi hơn.
                            </span>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="pb-5">
                <div className="text-4xl font-medium mb-8">Các câu hỏi thường gặp?</div>
                <div className="text-2xl font-medium mb-6">GrabFood là gì?</div>
                <ShowMoreText
                    /* Default options */
                    lines={4}
                    more={<Button className="h-11 w-full font-bold text-base text-gray-700 mt-4">Read More</Button>}
                    less={''}
                    anchorClass="my-anchor-css-class"
                    expanded={false}
                    truncatedEndingComponent={''}
                    className="whitespace-pre-line "
                >
                    <p>
                        Lunch, Bún Cá Chấm Gốc Đa - Vũ Thạnh for Dinner! We are here to satisfy your hunger with a wide
                        selection of merchant partners in Vietnam. GrabFood là dịch vụ Giao đồ ăn nhanh nhất tại Việt
                        Nam. Chúng tôi đã sắp xếp tất cả các món ăn, nhà hàng và thực phẩm yêu thích của bạn một cách
                        hợp lý để giúp bạn tìm được đồ ăn dễ dàng và nhanh chóng nhất có thể. Tìm và đặt món ăn yêu
                        thích trên khắp Việt Nam - đặt đồ ăn trực tuyến chỉ bằng vài thao tác, từ món Lifted Coffee &
                        Brunch cho bữa sáng, đến Maazi Indian – Nhà Hàng Ấn Độ cho bữa trưa, đến Bún Cá Chấm Gốc Đa – Vũ
                        Thạnh cho bữa tối! Hãy để chúng tôi xua tan cơn đói của bạn nhờ một loạt đối tác bán đồ ăn ở
                        Việt Nam.
                    </p>
                    <br />
                    <hr className="h-[0.5px] w-full bg-[#c5c5c559]" />
                    <div className="text-2xl font-medium mb-6">Làm cách nào để đặt đồ ăn ở Việt Nam?</div>
                    <p>Sau đây là cách đơn giản nhất để đặt đồ ăn qua GrabFood khi bạn ở Việt Nam:</p>
                    <ol>
                        <li className=" mb-4">
                            <span className="font-bold mr-1">1. Tìm kiếm nhà hàng hoặc món ăn yêu thích</span>
                            <span>
                                {' '}
                                - Nhập địa chỉ của bạn vào trang chủ. Xem các Nhà hàng và điểm ăn uống gần chỗ bạn trong
                                danh sách của GrabFood. Chọn nhà hàng yêu thích, duyệt hết thực đơn và chọn món ăn bạn
                                muốn đặt.
                            </span>
                        </li>
                        <li className=" mb-4">
                            <span className="font-bold mr-1">2. Kiểm tra & Thanh toán</span>
                            <span>
                                {' '}
                                - Sau khi chắc chắn rằng bạn đã đặt đầy đủ các món theo nhu cầu, hãy nhấp vào tab “ORDER
                                NOW” (ĐẶT MÓN NGAY) và nhập địa chỉ giao đồ ăn cuối cùng. Chọn phương thức thanh toán
                                phù hợp nhất với bạn và thanh toán.
                            </span>
                        </li>
                        <li className=" mb-4">
                            <span className="font-bold mr-1">3. Giao hàng</span>
                            <span>
                                - GrabFood đã thiết kế một hành trình phục vụ khách hàng liền mạch để bạn có thể thưởng
                                thức món ăn một cách trọn vẹn. Chúng tôi sẽ gửi cho bạn email và tin nhắn SMS tức thời
                                xác nhận đơn đặt hàng của bạn và thời gian giao hàng dự kiến. Sau đó chúng tôi sẽ giao
                                ngay đồ ăn cho bạn.
                            </span>
                        </li>
                    </ol>
                    <hr className="h-[0.5px] w-full bg-[#d1d1d13a]" />
                    <div className="text-2xl font-medium mb-6">GrabFood có cung cấp dịch vụ giao đồ ăn 24x7 không?</div>
                    <p>
                        Chúng tôi chỉ biết một điều duy nhất, đó là "đồ ăn", vậy nên tất nhiên chúng tôi cung cấp dịch
                        vụ này rồi. Xin lưu ý, mặc dù chúng tôi là đối tác giao đồ ăn phục vụ 24x7, nhưng một số nhà
                        hàng trong danh mục của chúng tôi có thể hạn chế giao đồ ăn khuya hoặc có thể không phục vụ đối
                        với các đơn đặt hàng. Tuy nhiên, chúng tôi đã liệt kê danh sách những nhà hàng phục vụ nhu cầu
                        ăn khuya của bạn trong mục Late Night Delivery (Giao hàng khuya)
                    </p>
                    <hr className="h-[0.5px] w-full bg-[#d1d1d13a]" />
                    <div className="text-2xl font-medium mb-6">GrabFood có chấp nhận tiền mặt không?</div>
                    <p>
                        Tất nhiên là có! GrabFood chấp nhận mọi hình thức thanh toán cho dịch vụ giao đồ ăn, bao gồm cả
                        tiền mặt khi giao hàng tại Việt Nam.
                    </p>
                    <hr className="h-[0.5px] w-full bg-[#d1d1d13a]" />
                    <div className="text-2xl font-medium mb-6">GrabFood tính phí giao đồ ăn như thế nào?</div>
                    <p>
                        Phí giao hàng của chúng tôi phụ thuộc vào nhiều yếu tố hoạt động như khoảng cách từ vị trí của
                        bạn đến nhà hàng. Bạn có thể kiểm tra phí giao hàng chính xác cần phải trả trước khi thanh toán
                        tại mục thanh toán trên ứng dụng Grab. Ngoài ra còn có phần “Free Delivery” (Giao hàng miễn phí)
                        liệt kê các nhà hàng gần chỗ bạn mà không tính phí giao hàng.
                    </p>
                    <hr className="h-[0.5px] w-full bg-[#d1d1d13a]" />
                    <div className="text-2xl font-medium mb-6">Những nhà hàng nào được liệt kê trong GrabFood?</div>
                    <p>
                        Chúng tôi có gì trên GrabFood? Chúng tôi tự hào có nhiều nhà hàng và món ăn nhất so với bất kỳ
                        ứng dụng giao đồ ăn nào ở Việt Nam. Bạn có thể lựa chọn trong số hàng nghìn nhà hàng trên
                        GrabFood Việt Nam. Bạn có thể đặt đồ ăn trực tuyến từ tất cả các điểm ăn uống yêu thích của
                        Burger King, Jollibee, KFC, McDonalds, Long John Silver, Pastamania, Dominos Pizza, Pizza Hut,
                        Subway!
                        <br />
                        <br />
                        GrabFood cũng có mã khuyến mãi, ưu đãi và giảm giá có hạn dành riêng cho các nhà hàng trong danh
                        mục. Bạn có thể được hưởng chiết khấu lớn và hàng loạt ưu đãi khác cho đơn đặt đồ ăn của mình.
                        Giờ thì đặt đồ ăn thôi!
                    </p>
                    <hr className="h-[0.5px] w-full bg-[#d1d1d13a]" />
                    <div className="text-2xl font-medium mb-6">
                        GrabFood có áp dụng giá trị đơn hàng tối thiểu không?
                    </div>
                    <p>
                        Có! Nhưng bạn có thể trả số tiền chênh lệch nếu giá trị đơn hàng của bạn nhỏ hơn số tiền đặt
                        hàng tối thiểu.
                    </p>
                </ShowMoreText>
            </div>
        </>
    );
}

export default IntroFood;
