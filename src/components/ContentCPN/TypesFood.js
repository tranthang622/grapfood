import { Col, Row } from 'react-bootstrap';
import drink from '../../assets/typeOfFood/typeDrink.jpeg';
import t from '../../utilities/i18n';
function TypesFood() {
    return (
        <Row className="mb-10">
            <div className="lg:text-4xl text-xl font-medium my-5">There's something for everyone!</div>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
            <Col xs="6" lg="3" className="">
                <div
                    className="w-full h-40 bg-cover bg-center overflow-hidden mb-2 rounded-md "
                    style={{ backgroundImage: `url(${drink})` }}
                ></div>
                <p className="text-lg font-bold">{t('iced-drinks')}</p>
            </Col>
        </Row>
    );
}

export default TypesFood;
