import { Col, Container, Row } from 'react-bootstrap';

function IntroApp() {
    return (
        <div className="w-full min-h-[30rem] bg-[#f7f7f7] ">
            <div className=""></div>
            <Container fluid="md">
                <Row>
                    <Col xs lg="6">
                        <div className=" p-16">
                            <div className="w-full flex justify-center mb-3">
                                <img src="https://food.grab.com/static/page-home/bottom-food-options.svg" alt="" />
                            </div>
                            <p className="text-center text-lg font-medium">Curated restaurants</p>
                            <p className="text-center">
                                From small bites to big meals, we won't limit your appetite. Go ahead and order all you
                                want.
                            </p>
                        </div>
                    </Col>
                    <Col xs lg="6">
                        <div className=" p-14 mt-3">
                            <div className="w-full flex justify-center mb-3">
                                <img
                                    src="	https://food.grab.com/static/images/ilus-cool-features-app.svg"
                                    alt=""
                                    className="w-40 h-36"
                                />
                            </div>
                            <p className="text-center text-lg font-medium">More cool features available on the app</p>
                            <p className="text-center">
                                Download Grab app to use other payment methods and enjoy seamless communication with
                                your driver.
                            </p>
                            <div className="flex justify-center ">
                                <img
                                    src="	https://food.grab.com/static/images/logo-appstore.svg"
                                    alt=""
                                    className="mr-4"
                                />
                                <img src="		https://food.grab.com/static/images/logo-playstore.svg" alt="" />
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default IntroApp;
