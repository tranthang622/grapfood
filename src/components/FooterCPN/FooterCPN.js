import { Col, Container, Row } from 'react-bootstrap';
import { Icon } from '@iconify/react';
import { FooterGrapFood } from '../../assets/svg/logoSVG';
function FooterCPN() {
    return (
        <div className="w-full min-h-[28rem] bg-[#00b14f] ">
            <Container fluid="md" className="pt-11">
                <FooterGrapFood />
                <hr className="w-full h-[1px] bg-[#ffffff] mt-3" />
                <Row>
                    <Col xs lg="3">
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Về GrabFood{' '}
                            </a>
                        </p>
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Về Grab{' '}
                            </a>
                        </p>
                        <a href="/" className="no-underline text-lg text-white font-medium">
                            Blog{' '}
                        </a>
                    </Col>
                    <Col xs lg="3">
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Mở quán trên GrabFood{' '}
                            </a>
                        </p>
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Trở thành tài xế Grab{' '}
                            </a>
                        </p>
                    </Col>
                    <Col xs lg="3">
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Trung tâm hỗ trợ
                            </a>
                        </p>
                        <p className="mb-4">
                            {' '}
                            <a href="/" className="no-underline text-lg text-white font-medium ">
                                Câu hỏi thường gặp
                            </a>
                        </p>
                    </Col>
                    <Col xs lg="3" className="flex">
                        <a href="/" className="no-underline text-3xl mr-5 text-white font-medium ">
                            <Icon icon="uil:facebook" />
                        </a>
                        <a href="/" className="no-underline text-3xl mr-5 text-white font-medium ">
                            <Icon icon="ion:logo-instagram" />
                        </a>
                        <a href="/" className="no-underline text-3xl  text-white font-medium ">
                            <Icon icon="ant-design:twitter-square-filled" />
                        </a>
                    </Col>
                </Row>
                <hr className="w-full h-[1px] bg-white my-5" />
                <div className="lg:flex justify-between">
                    <div className="flex  ">
                        <img src="	https://food.grab.com/static/images/logo-appstore.svg" alt="" className="mr-4" />
                        <img src="		https://food.grab.com/static/images/logo-playstore.svg" alt="" />
                    </div>
                    <div className="flex text-white text-sm ">
                        <p>2022 Grab</p>
                        <p className="ml-4">Câu hỏi thường gặp</p>
                        <p className="ml-4">Chính sách bảo mật</p>
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default FooterCPN;
