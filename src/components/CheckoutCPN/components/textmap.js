import React from 'react';
import GoogleMapReact from 'google-map-react';
import { Loader } from '@googlemaps/js-api-loader';
const key = 'AIzaSyBPDjB2qkV4Yxn9h0tGSk2X5uH6NKmssXw';
const AnyReactComponent = ({ text }) => <div>{text}</div>;
function TestMap() {
    const defaultProps = {
        center: {
            lat: 20.995301,
            lng: 105.870726,
        },
        zoom: 11,
    };

    return (
        <div style={{ height: '100vh', width: '100%' }}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyBPDjB2qkV4Yxn9h0tGSk2X5uH6NKmssXw' }}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
            >
                <AnyReactComponent lat={20.995301} lng={105.870726} text="My Marker" />
            </GoogleMapReact>
        </div>
    );
}

export default TestMap;
