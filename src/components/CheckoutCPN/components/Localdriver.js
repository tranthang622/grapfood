import { ConfigProvider, Input } from 'antd';
import { Col, Row } from 'react-bootstrap';
function Localdriver() {
    return (
        <div className="w-full flex justify-center mb-3 ">
            <div className="bg-white w-[40%] rounded-lg ">
                <div className="px-4 pt-4">
                    <h5>Giao đến </h5>

                    <div className="w-full my-6 h-[.5px] bg-[#eeeeee]"></div>
                </div>
                <div className="px-4">
                    <p className="text-xs mb-0 text-[#757575]">Delivery arrival time</p>
                    <p className="font-semibold">20 min (0,9 km away)</p>
                </div>

                <div className="w-full my-6  h-[.5px] bg-[#eeeeee]"></div>
                <div className=" p-4 w-full ">
                    <Row>
                        <Col lg="4">
                            {' '}
                            <div className="bg-[#22be22] w-full h-44 mr-9 "></div>
                        </Col>
                        <Col lg="8">
                            <div className="">
                                <div className="w-full ">
                                    <div className="text-xs text-[#868686] mb-2">Địa chỉ</div>
                                    <ConfigProvider
                                        theme={{
                                            token: {
                                                colorPrimary: '#00b96b',
                                                controlHeight: 46,
                                            },
                                        }}
                                    >
                                        <Input
                                            className="h-11 w-full mb-3"
                                            placeholder="Tìm món hoặc quán ăn ăn"

                                            // suffix={<Icon icon="ic:twotone-gps-fixed" color="#00b14f" />}
                                        />
                                    </ConfigProvider>
                                </div>
                            </div>
                            <div className="">
                                <div className="w-full ">
                                    <div className="text-xs text-[#868686] mb-2">Chi tiết địa chỉ</div>
                                    <ConfigProvider
                                        theme={{
                                            token: {
                                                colorPrimary: '#00b96b',
                                                controlHeight: 46,
                                            },
                                        }}
                                    >
                                        <Input
                                            className="h-11 w-full mb-3"
                                            placeholder="54 , Liễu Gai , P.Ngọc Khánh, Q.Ba Đình, Hà Nội, 1000, Việt Nam"

                                            // suffix={<Icon icon="ic:twotone-gps-fixed" color="#00b14f" />}
                                        />
                                    </ConfigProvider>
                                </div>
                            </div>
                            <div className="">
                                <div className="w-full ">
                                    <div className="text-xs text-[#868686] mb-2"> Ghi chú cho tài xế</div>
                                    <ConfigProvider
                                        theme={{
                                            token: {
                                                colorPrimary: '#00b96b',
                                                controlHeight: 46,
                                            },
                                        }}
                                    >
                                        <Input
                                            className="h-11 w-full mb-3"
                                            placeholder="Hãy gặp tôi tại sảnh "

                                            // suffix={<Icon icon="ic:twotone-gps-fixed" color="#00b14f" />}
                                        />
                                    </ConfigProvider>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>
    );
}

export default Localdriver;
