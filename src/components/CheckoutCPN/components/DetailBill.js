import { Icon } from '@iconify/react';
import { FormControl, MenuItem } from '@mui/material';
import { ConfigProvider, Select } from 'antd';
function DetailBill() {
    return (
        <div className="w-full flex justify-center mb-3">
            <div className="bg-white w-[40%] rounded-lg pb-3 ">
                <div className="px-4 pt-4">
                    <h5>Chi tiết thanh toán</h5>
                </div>

                <div className="w-full my-6  h-[.5px] bg-[#eeeeee]"></div>
                <div>
                    <div className=" px-4">
                        <div className="w-full ">
                            <div className="text-xs text-[#868686] mb-2">Phương thức thanh toán</div>
                            <FormControl sx={{ m: 1, minWidth: 500 }}>
                                <ConfigProvider
                                    theme={{
                                        token: {
                                            colorPrimary: '#00b96b',
                                            controlHeight: 46,
                                        },
                                    }}
                                >
                                    <Select
                                        suffixIcon={
                                            <Icon
                                                icon="material-symbols:keyboard-arrow-down-rounded"
                                                className="text-2xl"
                                            />
                                        }
                                        // menuItemSelectedIcon={<Icon icon="ic:baseline-calendar-month" />}
                                        defaultValue={'Tiền Mặt'}
                                        //   onChange={handleChange}
                                        // displayEmpty
                                        inputProps={{ 'aria-label': 'Without label' }}
                                        className={'h-12'}
                                    >
                                        <MenuItem value={30} disabled={true}>
                                            <div className={'flex text-base mt-2 '}>
                                                <Icon
                                                    icon="mdi:alpha-m-box"
                                                    className=" mr-3 text-[#516fe9] text-3xl"
                                                />{' '}
                                                <span className="mt-1">Phương thức chưa hỗ trợ</span>
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={10}>
                                            <div className={'flex text-base mt-2 '}>
                                                <Icon
                                                    icon="iconoir:money-square"
                                                    className=" mr-3 text-[#6e6e6e] text-2xl"
                                                />{' '}
                                                <span>Tiền mặt</span>
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={20}>
                                            {' '}
                                            <div className={'flex text-base mt-2 '}>
                                                <Icon
                                                    icon="ph:credit-card-light"
                                                    className=" mr-3 text-[#6e6e6e] text-3xl"
                                                />{' '}
                                                <span className="mt-1">3042</span>
                                            </div>
                                        </MenuItem>
                                    </Select>
                                </ConfigProvider>
                            </FormControl>
                        </div>
                        <div className="w-full ">
                            <div className="text-xs text-[#868686] mb-2">Hồ sơ</div>
                            <FormControl sx={{ m: 1, minWidth: 500 }}>
                                <ConfigProvider
                                    theme={{
                                        token: {
                                            colorPrimary: '#00b96b',
                                            controlHeight: 46,
                                        },
                                    }}
                                >
                                    <Select
                                        suffixIcon={
                                            <Icon
                                                icon="material-symbols:keyboard-arrow-down-rounded"
                                                className="text-2xl"
                                            />
                                        }
                                        // menuItemSelectedIcon={<Icon icon="ic:baseline-calendar-month" />}
                                        defaultValue={'Personal'}
                                        //   onChange={handleChange}
                                        // displayEmpty
                                        inputProps={{ 'aria-label': 'Without label' }}
                                        className={'h-12'}
                                    >
                                        <MenuItem value={20}>
                                            {' '}
                                            <div className={'flex text-base mt-2 '}>
                                                <Icon
                                                    icon="ph:user-rectangle-thin"
                                                    className=" mr-3 text-[#6e6e6e] text-3xl"
                                                />{' '}
                                                <span className="mt-1">Personal</span>
                                            </div>
                                        </MenuItem>
                                    </Select>
                                </ConfigProvider>
                            </FormControl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DetailBill;
