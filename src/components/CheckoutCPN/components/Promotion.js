import { Button, ConfigProvider, Input } from 'antd';
function Promotion() {
    return (
        <div className="w-full flex justify-center mb-3">
            <div className="bg-white w-[40%] rounded-lg pb-3 ">
                <div className="px-4 pt-4">
                    <h5>Khuyến mãi</h5>
                </div>

                <div className="w-full my-6  h-[.5px] bg-[#eeeeee]"></div>
                <div>
                    <div className=" px-4 flex">
                        <ConfigProvider
                            theme={{
                                token: {
                                    colorPrimary: '#00b96b',
                                    controlHeight: 46,
                                },
                            }}
                        >
                            <Input className="h-11 w-full mb-3" placeholder="Nhập mã khuyến mãi" />
                        </ConfigProvider>
                        <Button className="bg-[#00b14f] text-white w-96 h-12  text-lg font-semibold ml-10">
                            Áp dụng
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Promotion;
