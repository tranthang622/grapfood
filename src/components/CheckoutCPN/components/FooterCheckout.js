import { Icon } from '@iconify/react';
import userAsk from '../../../assets/logo/uerAsk.svg';
function FooterCheckout() {
    return (
        <div>
            <div className="w-full flex justify-center mb-3">
                <div className="bg-white w-[40%] rounded-lg p-3  flex ">
                    <div>
                        <div
                            className="w-32 h-32 bg-no-repeat bg-cover "
                            style={{ backgroundImage: `url(${userAsk})` }}
                        ></div>
                    </div>
                    <div className="ml-10 mt-4">
                        <p className="text-sm text-[#565656]">
                            Dùng phương thức thanh toán khác để nhận ưu đãi hấp dẫn. Chỉ có trên ứng dụng Grab.
                        </p>
                        <div className="flex">
                            <Icon icon="logos:google-play" className="text-2xl" />
                            <div className="flex ml-10">
                                <Icon icon="ion:logo-apple" className="text-2xl" />
                                <span className="ml-2">AppStore</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-full flex justify-center mb-6">
                <span>
                    <a href="/" className="text-sm text-[#8b8b8b] no-underline font-semibold">
                        © 2023 Grab
                    </a>
                </span>
                <span>
                    <a className="text-sm text-[#8b8b8b] no-underline font-semibold ml-6" href="/">
                        Câu hỏi thường gặp
                    </a>
                </span>
                <span>
                    <a className="text-sm text-[#8b8b8b] no-underline font-semibold ml-6" href="/">
                        Chính sách bảo mật
                    </a>
                </span>
            </div>
        </div>
    );
}

export default FooterCheckout;
