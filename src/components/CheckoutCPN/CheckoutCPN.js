import { Button } from 'antd';
import { useState } from 'react';
import Header from '../../layout/header/header';
import DetailBill from './components/DetailBill';
import FooterCheckout from './components/FooterCheckout';
import Localdriver from './components/Localdriver';
import Promotion from './components/Promotion';
import SummaryOder from './components/SummaryOder';

function CheckoutCPN() {
    const [count, setCount] = useState();

    return (
        <div>
            <Header />
            <div className="bg-white w-full h-40 text-center mt-28">
                <h1>Bước cuối cùng - Thanh toán</h1>
                <h4>Super Chicken - Tiệm Gà Online - Đào Tấn</h4>
            </div>
            <div className="bg-[#f7f7f7]  w-full pt-4 mb-20 pb-2">
                <Localdriver />
                <SummaryOder />
                <DetailBill />
                <Promotion />
                <FooterCheckout />
            </div>

            <div className="bg-white fixed bottom-0 h-28 w-full flex justify-center items-center  shadow-sm">
                <div>
                    <div className="text-xl ">Tổng Cộng</div>
                    <span className="text-2xl font-semibold">59.000 đ</span>
                    <span className="text-sm line-through text-[#6b6b6b] ml-2">66.000D</span>
                </div>
                <Button className="bg-[#00b14f] text-white w-96 h-12 ml-20  text-lg font-semibold">Đặt đơn</Button>
            </div>
        </div>
    );
}

export default CheckoutCPN;
