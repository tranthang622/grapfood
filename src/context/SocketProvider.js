import * as React from 'react';
import { SOCKETEVENT_TYPES } from '../constants/enumGlobal';
import { socket } from './socket';
import { useDispatch } from 'react-redux';
// import { updatePost } from '../redux/feature/postSlice'
export const SocketContext = React.createContext();

function SocketProvider({ children }) {
    const dispatch = useDispatch();

    React.useEffect(() => {
        socket.on('connect', () => {
            console.log('connect');
        });
        // socket.on(SOCKETEVENT_TYPES.SOCKET_POST_UPDATE, dataGot => {
        //   dispatch(updatePost(dataGot))
        // })
        socket.on(SOCKETEVENT_TYPES.TOKEN, (dataGot) => {
            console.log('🚀 ~ file: SocketProvider.js ~ line 13 ~ React.useEffect ~ dataGot', dataGot);
        });
        socket.on(SOCKETEVENT_TYPES.USER_ID, (dataGot) => {
            console.log('🚀 ~ file: SocketProvider.js ~ line 13 ~ React.useEffect ~ dataGot', dataGot);
        });
        socket.emit();
        return () => {
            socket.off('connect');
            socket.off('disconnect');
        };
    }, []);

    const value = { socket };
    console.log(value);
    return <SocketContext.Provider value={value}>{children}</SocketContext.Provider>;
}
export const useSocketIO = () => {
    const context = React.useContext(SocketContext);
    if (context === undefined) {
        throw new Error('useSocketIO must be used within a SocketProvider');
    }
    return context;
};

export default SocketProvider;
