import { io } from 'socket.io-client';
import { URL_SOCKET } from '../config/baseUrl';

export const socket = io(URL_SOCKET, {
    transports: ['websocket', 'polling'],
});
// export default class SocketTestScreen extends React.Component {
//   constructor(props) {
//     super(props)

//     this.initSocket()
//     this.state = {
//       messages: [
//         {
//           message: 'ok bắt đầu nhé!'
//         }
//       ],
//       rooms: [],
//       inputVisible: false
//     }
//   }

//   initSocket = () => {
//     console.log('basessss', API_DOMAIN)
//     // this.socketIo = io.connect(process.env.REACT_APP_API_SOCKET, {
//     this.socketIo = io(API_DOMAIN, {
//       transports: ['websocket', 'polling']
//     })
//     this.socketIo.on('connect', () => {
//       console.log(this.socketIo.connected) // true
//     })
//   }

//   componentDidMount() {
//     this.listenerSocket()
//   }

//   listenerSocket = () => {
//     console.log('starrttt')
//     this.socketIo.emit('CTA_CONNECTED', {
//       message: 'Connect roi nhe'
//     })

//     this.socketIo.onAny((eventName, data) => {
//       console.log('starrttt', { eventName, data })
//       this.appendMessage({
//         eventName,
//         data
//       })
//     })
//   }

//   appendMessage = msg => {
//     console.log('socket__ ATC_CONNECTED ', msg)
//     this.setState(prevState => {
//       return {
//         messages: [
//           {
//             ...msg,
//             ...{ createAt: new Date() }
//           }
//         ].concat(prevState.messages)
//       }
//     })
//   }
//   renderListMessages = () => {
//     return (
//       <div style={{ flex: 1, backgroundColor: 'darkgray', padding: 3 }}>
//         <ListGroup>
//           {this.state.messages.map((message, index) => {
//             return (
//               <ListGroup.Item as="li" key={index}>
//                 <div className="ms-2 me-auto">
//                   <div className="fw-bold">{JSON.stringify(message)}</div>
//                   {message?.createAt?.toString()}
//                 </div>
//               </ListGroup.Item>
//             )
//           })}
//         </ListGroup>
//       </div>
//     )
//   }

//   handleClose = removedTag => {
//     const rooms = this.state.rooms.filter(tag => tag !== removedTag)
//     this.setState(
//       {
//         rooms
//       },
//       () => {
//         this.socketIo.emit('CTA_LEAVEROOM', {
//           roomName: removedTag
//         })
//       }
//     )
//   }

//   showInput = () => {
//     this.setState({ inputVisible: true }, () => this.input.focus())
//   }

//   handleInputConfirm = e => {
//     const inputValue = e.target.value
//     let { rooms } = this.state
//     if (inputValue && rooms.indexOf(inputValue) === -1) {
//       rooms = [...rooms, inputValue]
//       this.socketIo.emit('CTA_JOINROOM', {
//         roomName: inputValue
//       })
//     }
//     console.log(rooms)
//     this.setState({
//       rooms,
//       inputVisible: false
//     })
//   }

//   saveInputRef = input => {
//     this.input = input
//   }

//   forMap = tag => {
//     const tagElem = (
//       <Badge bg="secondary">
//         {tag}{' '}
//         <Button
//           onClick={e => {
//             e.preventDefault()
//             this.handleClose(tag)
//           }}
//         >
//           x
//         </Button>
//       </Badge>
//     )
//     return (
//       <span key={tag} style={{ display: 'inline-block' }}>
//         {tagElem}
//       </span>
//     )
//   }

//   render() {
//     const { rooms, inputVisible } = this.state
//     const tagChild = rooms.map(this.forMap)
//     return (
//       <>
//         <div style={{ marginBottom: 16 }}>{tagChild}</div>
//         {inputVisible && (
//           <FormControl
//             ref={this.saveInputRef}
//             placeholder="nhập tên Room"
//             aria-label="Username"
//             aria-describedby="basic-addon1"
//             onBlur={this.handleInputConfirm}
//             onSubmit={this.handleInputConfirm}
//           />
//         )}
//         {!inputVisible && (
//           <Badge onClick={this.showInput} bg="secondary">
//             <FaPlus onClick={this.showInput} className="fs-2" />
//           </Badge>
//         )}
//         {this.renderListMessages()}
//       </>
//     )
//   }
// }
