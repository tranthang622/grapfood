import CheckoutCPN from '../components/CheckoutCPN/CheckoutCPN';
import DetailGrabFood from '../components/DetailGrabFood/DetailGrabFood';
import DetailRestaurants from '../components/DetailRestaurants/DetailRestaurants';
import config from '../config';
import Login from '../features/authen/pages/login';
import SignIn from '../features/authen/pages/singin';
import Body from '../layout/body/body';
// Don't need login
export const publicRouter = [
    { path: config.routes.home, component: Body },
    { path: config.routes.restaurant, component: DetailGrabFood },
    { path: config.routes.restaurant_food, component: DetailRestaurants },
    { path: config.routes.checkout, component: CheckoutCPN, layout: null },
    { path: config.routes.signUp, component: Login, layout: null },
    { path: config.routes.signIn, component: SignIn, layout: null },
    { path: config.routes.signIn, component: SignIn, layout: null },
];
