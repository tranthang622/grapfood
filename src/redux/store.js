import { configureStore } from '@reduxjs/toolkit';
import basketSlice from './silceRedux/basketReduce';

export const store = configureStore({
    reducer: {
        basket: basketSlice,
    },
});
