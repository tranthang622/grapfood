import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    totalBasket: [],
};
const basketSlice = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        addFood: (state, action) => {
            state.totalBasket.push(action.payload);
        },
    },
});
const { reducer, actions } = basketSlice;
export const { addFood } = actions;
export default reducer;
