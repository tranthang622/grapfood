import i18n from '../i18n';

const t = (text) => i18n.t(text);
export default t;
