import logo from './logo.svg';
import './App.css';
import Layout from './layout/layout';
import { Helmet } from 'react-helmet';
import i18n from './i18n';
import { useState } from 'react';
import LocaleContext from './context/LocaleContext';
import { BrowserRouter } from 'react-router-dom';
import PublicRoutes from './router/PublicRoutes';

function App() {
    console.log(i18n.language);
    const [locale, setLocale] = useState('en');
    i18n.on('languageChanged', () => setLocale(i18n.language));
    localStorage.setItem('mathang', 'Combogiare');
    return (
        <div className="App">
            <LocaleContext.Provider value={{ locale, setLocale }}>
                <Helmet>
                    <meta charset="utf-8" />
                    <title>GrabFood</title>
                    <meta name="description" content="This is main page" />
                </Helmet>
                {/* <Layout /> */}
                <BrowserRouter>
                    <PublicRoutes />
                </BrowserRouter>
            </LocaleContext.Provider>
        </div>
    );
}

export default App;
